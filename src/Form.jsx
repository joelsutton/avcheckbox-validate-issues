import React from 'react';
import { Label } from 'reactstrap';
import { AvCheckbox, AvCheckboxGroup, AvForm } from 'availity-reactstrap-validation';

export const Form = (props) => (
    <AvForm>
        <h1>Checkbox validation</h1>
        <AvCheckboxGroup
            name="options"
            label="Validate using 'required'"
            validate={{
                required: { value: true }
            }}
        >
            <Label>
                The <code>required</code> validator should enforce that at least one option is checked.
                However if you select multiple options, then unselect one, it becomes invalid even though unselecting another option then selecting it again makes the form valid.
            </Label>
            <AvCheckbox label="Option 1" />
            <AvCheckbox label="Option 2" />
            <AvCheckbox label="Option 3" />
        </AvCheckboxGroup>

        <AvCheckboxGroup
            name="options"
            label="Validate using 'min: 2'"
            validate={{
                min: { value: 2 }
            }}
        >
            <Label>
                The <code>min</code> validator is valid if you select enough options to make it valid and then deselect an option.
            </Label>
            <AvCheckbox label="Option 1" />
            <AvCheckbox label="Option 2" />
            <AvCheckbox label="Option 3" />
        </AvCheckboxGroup>
    </AvForm>
);
