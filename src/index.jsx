import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

import { Form } from './Form.jsx';

const rootElem = document.getElementById('app');

ReactDOM.render(
    <div className="container">
        <Form />
    </div>,
    rootElem
);
